package ClientDemo;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import ClientDemo.ClientDemo.FRealDataCallBack;
import ClientDemo.HCNetSDK.*;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.examples.win32.W32API.HWND;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.NativeLongByReference;

/**
 * @author bzz E-mail: ddnzero@126.com
 * @version 创建时间：2016-8-31 下午2:35:43 类说明
 */
public class MyClient {
	
	static HCNetSDK hCNetSDK = HCNetSDK.INSTANCE;
	static NativeLong lUserID;// 用户句柄
	public static NativeLong lPreviewHandle;//预览句柄
    public static NativeLongByReference m_lPort;//回调预览时播放库端口指针
    public static FRealDataCallBack fRealDataCallBack;//预览回调函数实现
    public static boolean bRealPlay = false;//是否在预览.
    public static PlayCtrl playControl = PlayCtrl.INSTANCE;
    
	static HCNetSDK.NET_DVR_IPPARACFG m_strIpparaCfg;// IP参数
	public static HCNetSDK.NET_DVR_DEVICEINFO_V30 m_strDeviceInfo;// 设备信息
	//public static HCNetSDK.NET_DVR_RealPlay_V30 m_str_RealPlay;// 设备回放句柄
	static HCNetSDK.NET_DVR_CLIENTINFO m_strClientInfo;// 用户参数

	
	
	public static String m_sDeviceIp = "192.168.1.12";//已登录设备的IP地址
	public static int m_sDevicePort = 8000;//已登录设备的端口
	public static String m_sDeviceName = "admin";//已登录设备账号
	public static String m_sDevicePassword = "cybhymlx888";//已登录设备密码
	
	public static void main(String[] args) {
		
		getIPCImage();
		
		/*boolean initSuc = hCNetSDK.NET_DVR_Init();
		if (!initSuc)return;
		
		m_strDeviceInfo = new HCNetSDK.NET_DVR_DEVICEINFO_V30();
		lUserID = hCNetSDK.NET_DVR_Login_V30("192.168.1.12", (short) 8000, "admin", "admin123", m_strDeviceInfo);
		long userID = lUserID.longValue();
		System.out.println(userID);
		CreateDeviceTree();
		StartDVRRecord();
		StopDVRRecord();*/
	}

	
	public static void getIPCImage(){
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			int $int = 0;
			boolean $flag = true;
			String $temp = "";
			
            public void run(){
            	//1.初始化设备
				boolean initSuc = hCNetSDK.NET_DVR_Init();
				if (initSuc) {
					System.out.println("ok");
					$int = hCNetSDK.NET_DVR_GetSDKVersion();
					System.out.println($int);
					$flag = hCNetSDK.NET_DVR_GetSDKState(new NET_DVR_SDKSTATE());
					System.out.println($flag);

					// 2.用户注册设备
					m_strDeviceInfo = new HCNetSDK.NET_DVR_DEVICEINFO_V30();
					lUserID = hCNetSDK.NET_DVR_Login_V30(m_sDeviceIp, (short) m_sDevicePort, m_sDeviceName, m_sDevicePassword, m_strDeviceInfo);
					long userID = lUserID.longValue();
					if (userID == -1) {
						System.out.println("fail");
					} else {
						System.out.println(userID+"  ok");
					}
					
					
					// 3.播放视频
					//hCNetSDK.NET_DVR_RealPlay_V30(lUserID, lpClientInfo, fRealDataCallBack_V30, pUser, bBlocked);
					
					//如果预览窗口没打开,不在预览
			        if (bRealPlay == false){
			            //获取窗口句柄
			            //HWND hwnd = new HWND(Native.getComponentPointer(panelRealplay));

			            //获取通道号
			            int iChannelNum = 1;//通道号

			            m_strClientInfo = new HCNetSDK.NET_DVR_CLIENTINFO();
			            m_strClientInfo.lChannel = new NativeLong(iChannelNum);

			            //在此判断是否回调预览,0,不回调 1 回调
						lPreviewHandle = hCNetSDK.NET_DVR_RealPlay_V30(lUserID, m_strClientInfo, null, null, true);
						//lPreviewHandle = hCNetSDK.NET_DVR_RealPlay_V30(lUserID, m_strClientInfo, fRealDataCallBack, null, true);

			            long previewSucValue = lPreviewHandle.longValue();

			            //预览失败时:
			            if (previewSucValue == -1) {
			            	System.out.println("预览失败");
			                return;
			            }
			            bRealPlay = true;
			        }
					
					
					
					//cleanup SDK
	                hCNetSDK.NET_DVR_Cleanup();
				}else{
	            	//cleanup SDK
	                hCNetSDK.NET_DVR_Cleanup();
	            }
            }
            
            

        });
		
		
		
	}
	
	
	/**
	 *  函数:CreateDeviceTree
     *	函数描述:建立设备通道数
	 */
	private static void CreateDeviceTree() {
		IntByReference ibrBytesReturned = new IntByReference(0);// 获取IP接入配置参数
		boolean bRet = false;

		m_strIpparaCfg = new HCNetSDK.NET_DVR_IPPARACFG();
		m_strIpparaCfg.write();
		Pointer lpIpParaConfig = m_strIpparaCfg.getPointer();
		bRet = hCNetSDK.NET_DVR_GetDVRConfig(lUserID, HCNetSDK.NET_DVR_GET_IPPARACFG, new NativeLong(0), lpIpParaConfig, m_strIpparaCfg.size(), ibrBytesReturned);
		m_strIpparaCfg.read();

		if (!bRet) {
			// 设备不支持,则表示没有IP通道
			for (int iChannum = 0; iChannum < m_strDeviceInfo.byChanNum; iChannum++) {
				System.out.println("Camera" + (iChannum + m_strDeviceInfo.byStartChan));
			}
		} else {
			// 设备支持IP通道
			for (int iChannum = 0; iChannum < m_strDeviceInfo.byChanNum; iChannum++) {
				if (m_strIpparaCfg.byAnalogChanEnable[iChannum] == 1) {
					System.out.println("Camera" + (iChannum + m_strDeviceInfo.byStartChan));
				}
			}
			for (int iChannum = 0; iChannum < HCNetSDK.MAX_IP_CHANNEL; iChannum++)
				if (m_strIpparaCfg.struIPChanInfo[iChannum].byEnable == 1) {
					System.out.println("IPCamera" + (iChannum + m_strDeviceInfo.byStartChan));
				}
		}
	}

	public static void StartDVRRecord() {
		m_strClientInfo = new HCNetSDK.NET_DVR_CLIENTINFO();
		m_strClientInfo.lChannel = new NativeLong(1);
		hCNetSDK.NET_DVR_StartDVRRecord(lUserID, m_strClientInfo.lChannel, new NativeLong(0));
	}

	private static void StopDVRRecord() {
		hCNetSDK.NET_DVR_StopDVRRecord(lUserID, m_strClientInfo.lChannel);
	}
	
	
	
	/*
     *内部类:   FRealDataCallBack
     *实现预览回调数据
     */
	class FRealDataCallBack implements HCNetSDK.FRealDataCallBack_V30 {
		// 预览回调
		public void invoke(NativeLong lRealHandle, int dwDataType, ByteByReference pBuffer, int dwBufSize, Pointer pUser) {
			//HWND hwnd = new HWND(Native.getComponentPointer(panelRealplay));SWING
			
			switch (dwDataType) {
			case HCNetSDK.NET_DVR_SYSHEAD: // 系统头
				if (!playControl.PlayM4_GetPort(m_lPort)){ // 获取播放库未使用的通道号
					break;
				}

				if (dwBufSize > 0) {
					// 设置实时流播放模式
					if (!playControl.PlayM4_SetStreamOpenMode(m_lPort.getValue(), PlayCtrl.STREAME_REALTIME)){
						break;
					}
					 // 打开流接口
					if (!playControl.PlayM4_OpenStream(m_lPort.getValue(), pBuffer, dwBufSize, 1024 * 1024)){
						break;
					}
					// 播放开始
					//if (!playControl.PlayM4_Play(m_lPort.getValue(), hwnd)){
						//break;
					//}
				}
			case HCNetSDK.NET_DVR_STREAMDATA: // 码流数据
				if ((dwBufSize > 0) && (m_lPort.getValue().intValue() != -1)) {
					if (!playControl.PlayM4_InputData(m_lPort.getValue(), pBuffer, dwBufSize)){ // 输入流数据
						break;
					}
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
}
